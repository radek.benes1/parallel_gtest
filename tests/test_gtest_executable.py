import pytest

from parallel_gtest.gtest_executable import GTestExecutable


@pytest.mark.parametrize(
    "path, expected",
    [
        ("C:/Projects/my_project/build/mytest.exe", "mytest.exe"),
        ("build/mytest_relative.exe", "mytest_relative.exe"),
    ],
)
def test_binary_name_win(path, expected, mocker):
    mocker.patch("parallel_gtest.gtest_executable.is_windows", return_value=True)
    mocker.patch("os.path.exists", return_value=True)

    gtest_executable = GTestExecutable(path)
    assert gtest_executable.binary_name == expected


@pytest.mark.parametrize(
    "path, expected",
    [
        ("home/projects/build/mytest", "./mytest"),
        ("build/mytest_relative", "./mytest_relative"),
    ],
)
def test_binary_name_lin(path, expected, mocker):
    mocker.patch("parallel_gtest.gtest_executable.is_windows", return_value=False)
    mocker.patch("os.path.exists", return_value=True)

    gtest_executable = GTestExecutable(path)
    assert gtest_executable.binary_name == expected
