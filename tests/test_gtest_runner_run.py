import pytest

from parallel_gtest.gtest_exceptions import BadTestBinary
from parallel_gtest.gtest_runner import GTestRunner

# from bin_fixtures import bin_path, bin_path_fail


def test_run_test(bin_path):
    gtest_runner = GTestRunner(bin_path)
    gtest_listing = gtest_runner.list_tests()

    test_name = gtest_listing.test_names[0]

    test_result = gtest_runner.run_test(test_name)

    assert test_name == test_result.test_name
    assert 11 == len(test_result.output)


def test_run_test_bad_binary(bin_path):
    with pytest.raises(BadTestBinary, match="Bad binary"):
        gtest_runner = GTestRunner(bin_path + "_non_existing")
        gtest_runner.run_test("a")


def test_run_test_fail(bin_path_fail):
    gtest_runner = GTestRunner(bin_path_fail)
    gtest_listing = gtest_runner.list_tests()

    test_name = gtest_listing.test_names[0]

    result = gtest_runner.run_test(test_name)
    assert not result.is_success


def test_run_tests(bin_path):
    gtest_runner = GTestRunner(bin_path)
    gtest_listing = gtest_runner.list_tests()

    test_results = gtest_runner.run_tests(gtest_listing.test_names)

    assert len(test_results) == gtest_listing.test_count
    test_names_from_res = [t.test_name for t in test_results]
    assert gtest_listing.test_names == test_names_from_res


def test_run_tests_fail(bin_path_fail):
    gtest_runner = GTestRunner(bin_path_fail)
    gtest_listing = gtest_runner.list_tests()

    results = gtest_runner.run_tests(gtest_listing.test_names, 3)

    fails = [not result.is_success for result in results]
    assert any(fails)


@pytest.mark.parametrize("repeat_cnt", [2, 3, 5])
def test_run_test_repeat(bin_path, repeat_cnt):
    def _single_test_len() -> int:
        gtest_runner = GTestRunner(bin_path)
        gtest_listing = gtest_runner.list_tests()
        test_name = gtest_listing.test_names[0]
        test_result = gtest_runner.run_test(test_name)

        single_test_len = len(test_result.output)
        return single_test_len

    single_test_len = _single_test_len()

    # There are 2 lines about global environment
    #   Global test environment set-up.
    #   Global test environment tear-down
    cnt_of_global_specific_lines = 2
    single_test_len = single_test_len - cnt_of_global_specific_lines

    gtest_runner = GTestRunner(bin_path, other_params=f"--gtest_repeat={repeat_cnt}")
    gtest_listing = gtest_runner.list_tests()
    test_name = gtest_listing.test_names[0]

    test_result_repeat = gtest_runner.run_test(test_name)

    assert test_name == test_result_repeat.test_name

    # for each test, there is written 3 redundant lines
    # <empty line>
    # Repeating all tests (iteration 1) . . .
    # <empty line>
    cnt_of_repeat_specific_lines = 3

    expected_len = (
        single_test_len * repeat_cnt
        + cnt_of_repeat_specific_lines * repeat_cnt
        + cnt_of_global_specific_lines
    )

    assert expected_len == len(test_result_repeat.output)
