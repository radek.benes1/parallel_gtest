from parallel_gtest.gtest_structure import GTestList, GTestTestSuite


def test_test_list():
    test_list = GTestList()

    test_suite = GTestTestSuite("ts1")
    test_suite.add_test("t1")
    test_suite.add_test("t2")

    test_list.add_tes_suite(test_suite)

    assert str(test_list) == "ts1\n t1\n t2\n"
