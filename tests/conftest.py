import os

import pytest

from parallel_gtest.common_tools import is_windows


@pytest.fixture(scope="session")
def bin_path():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if is_windows():
        return os.path.join(dir_path, "../gtest_bin/build/Release/basic_test.exe")
    else:
        return os.path.join(dir_path, "../gtest_bin/build/basic_test")


@pytest.fixture(scope="session")
def bin_path_fail():
    dir_path = os.path.dirname(os.path.realpath(__file__))
    if is_windows():
        return os.path.join(dir_path, "../gtest_bin/build/Release/basic_test_fail.exe")
    else:
        return os.path.join(dir_path, "../gtest_bin/build/basic_test_fail")
