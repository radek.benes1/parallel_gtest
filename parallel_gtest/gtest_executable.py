import os

from .common_tools import is_windows
from .gtest_exceptions import BadTestBinary


class GTestExecutable(object):
    def __init__(self, binary: str):
        if not os.path.exists(binary):
            raise BadTestBinary("Bad binary", binary)

        self._working_directory = os.path.dirname(binary)

        if is_windows():
            self._binary_name = os.path.basename(binary)
        else:
            self._binary_name = "./" + os.path.basename(binary)

    @property
    def binary_name(self):
        return self._binary_name

    @property
    def working_directory(self):
        return self._working_directory
