from typing import List


class GTestTestSuite(object):
    def __init__(self, suite_name: str):
        self._suite_name = suite_name
        self._tests: List[str] = []

    def add_test(self, test: str):
        self._tests.append(test)

    @property
    def tests(self):
        return [f"{self._suite_name}.{test}" for test in self._tests]

    @property
    def suite_name(self):
        return self._suite_name

    def __str__(self) -> str:
        ret_str = f"{self._suite_name}\n"
        for test in self._tests:
            ret_str += f" {test}\n"

        return ret_str


class GTestList(object):
    def __init__(self):
        self._test_suites = []

    def add_tes_suite(self, test_suite: GTestTestSuite):
        self._test_suites.append(test_suite)

    def last_test_suite_added(self) -> GTestTestSuite:
        return self._test_suites[len(self._test_suites) - 1]

    @property
    def test_suite_names(self) -> List[str]:
        return [test_suite.suite_name for test_suite in self._test_suites]

    @property
    def test_names(self) -> List[str]:
        all_tests = []

        for test_suite in self._test_suites:
            all_tests += test_suite.tests

        return all_tests

    @property
    def test_suites_count(self) -> int:
        return len(self._test_suites)

    @property
    def test_count(self) -> int:
        return len(self.test_names)

    def __str__(self) -> str:
        ret_str = ""
        for ts in self._test_suites:
            ret_str += str(ts)

        return ret_str
