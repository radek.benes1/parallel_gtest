#!/usr/bin/env python

import argparse

from parallel_gtest.common_tools import format_output
from parallel_gtest.gtest_runner import GTestRunner


def main():
    parser = argparse.ArgumentParser("test binary")
    parser.add_argument("path", help="path to gtest binary to be tested")
    parser.add_argument("--cpu", default=4, type=int, help="number of cpus to use")
    parser.add_argument(
        "--single", action="store_true", help="if selected run tests one by one."
    )
    parser.add_argument("--filter", type=str)
    parser.add_argument("--show-out", choices=["no", "console", "file"], default="no")
    parser.add_argument("--xml-out", type=str, required=False, help="path to output xml")
    args, unknown = parser.parse_known_args()

    other_args = " ".join(unknown)

    binary = args.path

    gtest_runner = GTestRunner(
        binary, other_params=other_args, filter=args.filter, xml_out=args.xml_out
    )

    gtest_lists = gtest_runner.list_tests()

    test_names = gtest_lists.test_names if args.single else gtest_lists.test_suite_names
    test_results = gtest_runner.run_tests(test_names, args.cpu)

    for test_result in test_results:
        if not test_result.is_success:
            print(format_output(test_result.output))

    if args.show_out == "console":
        for test_result in test_results:
            print(format_output(test_result.output))

    fails = [not result.is_success for result in test_results]
    if any(fails):
        exit(1)


if __name__ == "__main__":
    main()
