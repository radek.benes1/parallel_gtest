# developement

## pipenv

pipenv for developement is provided

```
pipenv install --dev
pipnev shell
```
---
**NOTE**
on windows, there is necessary to install `colorama` and `atomicwrites`.

---

## pre-commit
install pre-commit hook to ensure code rules.

```
pre-commit install
```

code will be checked in every commit.


## build unit test examples

on linux:
```
mkdir gtest_bin/build
cd gtest_bin/build
cmake ..
cmake --build ..
```

on windows:
```
cmake  -DCMAKE_CONFIGURATION_TYPES="Release" --build .
cmake  --build . --config Release
l_gtest\gtest_bin\build>Release
```

run `parallel-gtest`

```
parallel-gtest gtest_bin/build/basic_test
parallel-gtest gtest_bin/build/basic_test_fail
```


## python tests

unit test examples should be builded

```
pytest tests/*.py --cov=parallel_gtest --cov-report html --cov-report html --cov-report term
```


## TODO:
* better cli
* cli help can suggest help from binary
