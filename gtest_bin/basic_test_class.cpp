#include "gtest/gtest.h"

class MultiplyTest : public ::testing::Test {
 protected:
  virtual void SetUp() {
  }

  virtual void TearDown() {
  }
};

TEST_F(MultiplyTest, TwoValues) {
    EXPECT_EQ(20, 4 * 5);
    EXPECT_EQ(6, 2 * 3);
}

TEST_F(MultiplyTest, ThreeValues) {
    EXPECT_EQ(40, 4 * 5 * 2);
    EXPECT_EQ(24, 2 * 3 * 4);
}
