#include <gtest/gtest.h>

#include <string>
using std::string;
#include <chrono>   // NOLINT
#include <thread>   // NOLINT

TEST(IntCompare, Exact) {
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    EXPECT_EQ(5, 5);
}

TEST(IntCompare, GT) {
    std::this_thread::sleep_for(std::chrono::milliseconds(500));
    EXPECT_GT(6, 5);
}

TEST(IntCompare, Near) {
    std::this_thread::sleep_for(std::chrono::milliseconds(700));
    EXPECT_NEAR(5, 6, 2);
}
