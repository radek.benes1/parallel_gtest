#include <gtest/gtest.h>

#include <string>
using std::string;

TEST(IntCompare, Exact) {
    EXPECT_EQ(5, 5);
}

TEST(IntCompare, GT) {
    EXPECT_GT(6, 5);
}

TEST(IntCompare, Near) {
    EXPECT_NEAR(5, 6, 2);
}
